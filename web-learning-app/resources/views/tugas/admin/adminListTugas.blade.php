@extends('layouts.mastersLayouts')

@section('tittle_content')
    List tugas
@endsection

@section('content_page')
<div class="container-fluid">
    <div class="card">
      <div class="card-body">
        <div class="table-responsive">

            <table class= "table table-borderless w-fixed">
                <caption>list kelas</caption>
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">kelas</th>
                        <th scope="col">action</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($classes as $kelas =>$kls)
                        <tr>
                            <td>
                                {{$kelas+1}}
                            </td>
                            <td>
                                {{$kls->name}}
                            </td>
                            <td width="200">
                                <form action="/admin/kelas/create" method="POST">
                                    @csrf
                                    
                                    <button type="submit" name="kelas_id" value="{{$kls->id}}" class="btn btn-primary">Masuk ke tugas</button>
                                </form>

                            </td>
                        </tr>
                    @empty
                        <p>Belum ada kelas yang dibuat</p>
                    @endforelse
                </tbody>
            </table>

        </div>
        
      </div>
    </div>
  </div>
@endsection